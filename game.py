
# Instructions for the human
print("Think of a whole number betweeb 1 and 50.")

# generate a random number and assign to the variable guess
from random import randint

# Initialize the high and the low
low = 1
high = 50

# range(5)
range_one = range(5)

# Guess 5 times
for guess_number in range_one: 
    
    # Generate a random number
    guess = randint(low, high) 

    # Print the guess
    print("The computer guessed: ", guess)

    # Ask for a response from the human user
    response = input("Was your number higher, lower or equal.")

    # Output whether the answers match, the guess was too low,
    # or if the guess was too high.
    if response == "equal":
        print("Our answers matched! Yay!")
        
        # break out of the for statement if the answers are equal.
        break
    elif response == "higher":
        print("My guess was too low!")

        # Adjust the low if the computer's guess was too low.
        low = guess + 1
    elif response == "lower":
        print("My guess was too high!")

        # Adjust the high if the computer's guess was too high. 
        high = guess - 1

